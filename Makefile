SOURCE = main.c
ELF = main.elf
HEX = main.hex

$(ELF): $(SOURCE)
	avr-gcc -mmcu=atmega128 $< --output=$@

$(HEX): $(ELF)
	avr-objcopy -O ihex $< $@ --strip-all --strip-debug --strip-unneeded

flash: $(HEX)
	avrdude -p m128rfa1 -c stk500 -P /dev/ttyACM0 -U flash:w:$(HEX):a

clean:
	rm $(HEX) $(ELF)
