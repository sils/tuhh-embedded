#include <stdint.h>
#include <avr/io.h>

void wait_ms(void) {
    uint16_t i;
    // 16000 cycles per ms, one loop is 19 cycles
    for (i = 842; i > 0 ; i--) {
        // prevent code optimization by using inline assembler
        asm volatile ( "nop" );
    }
}

void wait(uint16_t ms) {
    for(; ms > 0; ms--) {
        wait_ms();
    }
}

int main(void) {
    DDRG |= 0x02;

    while (1) {
        wait(1000);
        PORTG ^= 0x02;
    }

    return 0;
}
